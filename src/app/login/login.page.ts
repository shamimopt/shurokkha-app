import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastController, ModalController } from '@ionic/angular';
import { SuccessPage } from '../success/success.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

  constructor(private http: HttpClient, private toastController: ToastController, public modalCtrl: ModalController) { }

  ngOnInit() { }

  user = {
    bp_id: '',
    password: ''
  }

  response: any;

  validateData(){
    if(this.user.bp_id.length < 1){
      this.presentToast('BP ID is required !');
      return false;
    }
    if(this.user.password.length < 1){
      this.presentToast('Password is required !');
      return false;
    }
    return true;
  }

  checkLogin() {
    this.validateData();
    if(this.validateData()){
      this.http.post('https://expactivation.com/shurokkha/public/api/bp/login', {
        bp_id: this.user.bp_id,
        password: this.user.password,
        vers: 1
      })
      .subscribe((response) => {
        this.response = response;
        if(this.response.response.status == 200){
          this.presentToast('Login Successfull !')
        }
        else if(this.response.response.status == 400){
          this.presentToast(this.response.response.message);
        }
        else{
          this.presentToast('Something went wrong, try again later !');
        }
      });
    }
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  async getActivity(bp_id){
    const modal = await this.modalCtrl.create({
      component: SuccessPage,
      componentProps: {
        title: 'Login Page',
        bp_id: bp_id
      }
    });
    return await modal.present();
  }
}