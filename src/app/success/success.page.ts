import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-success',
  templateUrl: './success.page.html',
  styleUrls: ['./success.page.scss'],
})
export class SuccessPage implements OnInit {

  @Input() bp_id;

  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }

   dismiss(){
      this.modalController.dismiss({
        'dismissed': true
      });
    }

}
