import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { LoadingController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { SuccessPage } from '../success/success.page';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {

  constructor(private toastController: ToastController, public modalController: ModalController, private storage: Storage, private camera: Camera, public loadingController: LoadingController, private modalCtrl: ModalController, private sanitizer: DomSanitizer, public geolocation: Geolocation, private http: HttpClient){}

  ngOnInit() {}

  latitude: any;
  longitude: any;
  user = {
    bp_id: '',
    password: ''
  }
  user_info = {
    name: '',
    mobile: '',
    otp: ''
  }
  error: string = '';
  waitmsg: string = '';
  loggedIn: any = false;
  response: any;
  activity: any;

  validateData(){
    if(this.user.bp_id.length < 1){
      this.presentToast('BP ID is required !');
      return false;
    }
    if(this.user.password.length < 1){
      this.presentToast('Password is required !');
      return false;
    }
    return true;
  }

  checkLogin() {
    this.validateData();
    if(this.validateData()){
      this.http.post('https://expactivation.com/shurokkha/public/api/bp/login', {
        bp_id: this.user.bp_id,
        password: this.user.password,
        vers: 1
      })
      .subscribe((response) => {
        this.response = response;
        if(this.response.response.status == 200){
          this.presentToast('Login Successfull !');
          this.loggedIn = true;
        }
        else if(this.response.response.status == 400){
          this.presentToast(this.response.response.message);
        }
        else{
          this.presentToast('Something went wrong, try again later !');
        }
      });
    }
  }

  requestOTP() {
    if(this.user_info.mobile == '')
    {
      this.presentToast("All felds are required !");
      return false;
    }
    
    let phoneno = /^\(?([0]{1})\)?([1]{1})\)?([3-9]{1})\)?([0-9]{8})$/;
    let plus88 = /^\(?([+]{1})\)?([8]{1})\)?([8]{1})\)?([0-9]{11})$/;
    let only88 = /^\(?([8]{1})\)?([8]{1})\)?([0-9]{11})$/;
    let zero = /^\(?([1]{1})\)?([3-9]{1})\)?([0-9]{8})$/;

    if(this.user_info.mobile.match(plus88))
    {
      this.presentToast("Input without (+88) 'ex: 013****' ");
      return false;
    }
    if(this.user_info.mobile.match(only88))
    {
      this.presentToast("Input without (88) 'ex: 013****' ");
      return false;
    }
    if(this.user_info.mobile.match(zero))
    {
      this.presentToast("Input with (01) 'ex: 013****' ");
      return false;
    }

    this.http.post('https://expactivation.com/shurokkha/public/api/bp/request-otp', {
      mobile: this.user_info.mobile,
      vers: 1
    })
    .subscribe((response) => {
      this.response = response;
      if(this.response.response.status == 200){
        this.presentToast(this.response.response.message);
      }
      else if(this.response.response.status == 400){
        this.presentToast(this.response.response.message);
      }
      else{
        this.presentToast('Something went wrong, try again later !');
      }
    });
  }


  checkRegistration(){
    if(this.user_info.name  == '' || this.user_info.mobile
      == '' || this.user_info.otp  == '' )
    {
      this.presentToast("All felds are required !");
      return false;
    }
    
    let phoneno = /^\(?([0]{1})\)?([1]{1})\)?([3-9]{1})\)?([0-9]{8})$/;
    let plus88 = /^\(?([+]{1})\)?([8]{1})\)?([8]{1})\)?([0-9]{11})$/;
    let only88 = /^\(?([8]{1})\)?([8]{1})\)?([0-9]{11})$/;
    let zero = /^\(?([1]{1})\)?([3-9]{1})\)?([0-9]{8})$/;

    if(this.user_info.mobile.match(plus88))
    {
      this.presentToast("Input without (+88) 'ex: 013****' ");
      return false;
    }
    if(this.user_info.mobile.match(only88))
    {
      this.presentToast("Input without (88) 'ex: 013****' ");
      return false;
    }
    if(this.user_info.mobile.match(zero))
    {
      this.presentToast("Input with (01) 'ex: 013****' ");
      return false;
    }

    this.presentLoading('Please wait...');
    this.http.post('https://expactivation.com/shurokkha/public/api/bp/registration', {
      name: this.user_info.name,
      mobile: this.user_info.mobile,
      otp: this.user_info.otp,
      bp_id: this.user.bp_id,
      vers: 1,
      date : new Date ((new Date((new Date(new Date())).toISOString() )).getTime() - ((new Date()).getTimezoneOffset()*60000)).toISOString().slice(0, 19).replace('T', ' ')
    })
    .subscribe((response) => {
      this.hideLoading();
      this.response = response;
      if(this.response.response.status == 200){
        this.response = response;
        this.clearUserInfo();
        this.presentToast(this.response.response.message);
      }
      else if(this.response.response.status == 400){
        this.presentToast(this.response.response.message);
      }
      else{
        this.presentToast("Server error");
      }
    });
  }

  loggedOut(){
    this.storage.set('user_data',JSON.stringify([]));
    this.clearUserInfo();
  }

  clearUserInfo(){
    this.user_info.name = '';
    this.user_info.mobile = '';
    this.user_info.otp = '';
  }

  async presentLoading(msg){
    const loading = await this.loadingController.create({
      message: msg,
      spinner: 'crescent'
    });
    await loading.present();
  }

  hideLoading(){
    this.loadingController.dismiss();
  }


  async presentToast(msg){
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  async showSuccess(bp_id){
    const modal = await this.modalCtrl.create({
      component: SuccessPage,
      componentProps: {
        title: 'Login Page',
        bp_id: bp_id
      }
    });
    return await modal.present();
  }

}